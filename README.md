# Teaset

## 项目介绍

- uni-app 是一个使用 Vue.js 开发跨平台应用的前端框架。
- 开发者通过编写 Vue.js 代码，uni-app 将其编译到iOS、Android、微信小程序等多个平台，保证其正确运行并达到优秀体验。
- uni-app 继承自 Vue.js，提供了完整的 Vue.js 开发体验。
- uni-app 组件规范和扩展api与微信小程序基本相同。
- 有一定 Vue.js 和微信小程序开发经验的开发者可快速上手 uni-app ，开发出兼容多端的应用。



**Teaset**是一个uniapp开源组件库，旨在推动uniapp的普及，收集整理社区常用的资源加工而成。

仓库地址：[https://gitee.com/zengqs/teaset](https://gitee.com/zengqs/teaset)

项目案例使用的数据源是本人打理的贝店的数据，各位亲多多支持下。贝店是一个类似于京东、天猫的购物平台，免费注册，首单送10元体验券。任何人都可申请成为店主，开店购物更实惠，每单返现。

我的贝店：[贺兰小铺](https://m.beidian.com/shop/shopkeeper.html?shop_id=682731)，店铺邀请码：**690638**。

开店指南：[http://beidian.zengqs.com](http://beidian.zengqs.com)


## 项目联系信息

1. 项目讨论QQ群:631951344
3. QQ:1024343803

## 安装教程

1. 参考本示例程序，将components目录及子目录下的所有文件复制到你新建的项目中。
2. 组件的全局注册请参考本示例程序根目录下的main.js和App.vue文件的代码

## 使用说明

1. 参考pages/example/teaset/文件，每一个组件有对应的案例的基础用法和高级案例。

* 仓库地址：https://gitee.com/zengqs/teaset

2. Uni-app教程 http://doc.zengqs.com
> Uni-app教程中包含Teaset的文档

## 教学资源

- [uni-app 官方文档](https://uniapp.dcloud.io)
- 课堂录像：[https://pan.baidu.com/s/1Ni5JxEt9pfY1tY7gu90J7A](https://pan.baidu.com/s/1Ni5JxEt9pfY1tY7gu90J7A), 密码：qf0i


## 演示界面
![](./screenshot/shot-08.png)
![](./screenshot/shot-03.jpg)
![](./screenshot/shot-04.jpg)
![](./screenshot/shot-06.jpg)
![](./screenshot/shot-05.jpg)
![](./screenshot/shot-07.jpg)
![](./screenshot/shot-01.jpg)
![](./screenshot/shot-02.jpg)



