import store from '@/store'

// #ifdef H5
// import Fly from 'flyio/dist/npm/fly'
const fly = require('flyio')
// #endif

// #ifndef H5
import Fly from 'flyio/dist/npm/wx'
const fly = new Fly();
// #endif



fly.interceptors.request.use((request) => {
	uni.showLoading({
		title: '正在努力加载中...'
	});

request.baseURL = 'http://127.0.0.1:8360/admin';
	// request.baseURL = 'http://192.168.31.90:8360/admin'; //局域网内手机调试必须设置在同一个网段
	// request.baseURL = 'http://119.147.44.180:8360/admin';
	// request.baseURL = 'http://www.zengqs.com:8360/admin';

	const token = store.getters['token/getToken'];
	if (token) {
		//给所有请求添加自定义header
		request.headers["Authorization"] = token;
	}

	// 防止缓存
	if (request.method.toUpperCase() === 'POST' && request.headers['Content-Type'] !== 'multipart/form-data') {
		request.body = {
			...request.body,
			_t: Date.parse(new Date()) / 1000
		}
	} else if (request.method.toUpperCase() === 'GET') {
		request.params = {
			_t: Date.parse(new Date()) / 1000,
			...request.params
		}
	}
	return request
})

fly.interceptors.response.use(function(response) { //不要使用箭头函数，否则调用this.lock()时，this指向不对 
	let errmsg = '';
	const data = response.data;
	if (!data || typeof data !== 'object') {
		errmsg = '服务器响应格式错误';
		uni.showToast({
			title: errmsg,
			icon: 'none'
		})
	} else {
		const errno = data.errno;
		switch (errno) {
			case 1001:
				// 数据检验未通过
				for (const i in data.data) {
					errmsg += data.data[i] + ';'
				}
				break;
			default:
				errmsg = data.errmsg;
				break
		}
		if (errmsg !== '' && errno !== 0) {
			uni.showToast({
				title: errmsg,
				icon: 'none'
			})
		}
		if (errmsg !== '' && errno === 0) {
			uni.showToast({
				title: errmsg,
				icon: 'none'
			})
		}
	}
	uni.hideLoading();
	return response.data; //只返回业务数据部分
}, function(err) {
	//return Promise.resolve("ssss")
	// console.error("error-interceptor:" + JSON.stringify(err))

	let errmsg = err.message;
	switch (err.status) {
		case 0:
			errmsg = "网络连接错误";
			uni.showToast({
				title: errmsg,
				icon: 'none'
			})
			break;
		case 401:
			store.dispatch('logout');
			uni.redirectTo({
				url: '/pages/auth/login'
			})
			break;
		default:
			uni.showToast({
				title: errmsg,
				icon: 'none'
			})
	}
})

export default fly
