import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		/**
		 * 是否需要强制登录
		 */
		forcedLogin: false,
		hasLogin: false,
		userName: "",

		cartList: [], //购物车列表
	},
	mutations: {
		login(state, userName) {
			state.userName = userName || '新用户';
			state.hasLogin = true;
		},
		logout(state) {
			state.userName = "";
			state.hasLogin = false;
		},

		addCart(state, payload) {
			// 			let value = payload;
			// 			let count = 1;
			// 			if (payload.hasOwnProperty('count')) {
			// 				count = payload.count;
			// 				value = payload.value;
			// 			}

			let {
				value,
				count
			} = payload;

			let product_id = value.product_id;
			const isAdded = state.cartList.find(e => e.product_id === product_id);
			if (isAdded) {
				isAdded.count += count;
			} else {
				state.cartList.push({
					product_id: product_id, //商品唯一id
					count: count, //购买数量
					checked: true, //是否选中支付
					value: value //商品详细信息
				})
			}

			uni.setStorage({
				key: 'cart',
				data: state.cartList,
				success: function() {
					//console.log('数据缓存成功');
				}
			});
			//console.log('$store.state.cartList:' + JSON.stringify(state.cartList));
		},
		editCartCount(state, payload) {
			const product = state.cartList.find(item => item.product_id === payload.product_id);
			product.count += payload.count;

			uni.setStorage({
				key: 'cart',
				data: state.cartList,
				success: function() {
					// console.log('数据缓存成功');
				}
			});
		},
		deleteCart(state, product_id) {
			const index = state.cartList.findIndex(item => item.product_id === product_id);
			state.cartList.splice(index, 1);

			uni.setStorage({
				key: 'cart',
				data: state.cartList,
				success: function() {
					// console.log('数据缓存成功');
				}
			});

		},
		//改变购物车中项目选中状态
		editCartItemStatus(state, payload) {
			let product = state.cartList.find(item => item.product_id === payload.product_id);

			if (payload.hasOwnProperty('checked')) {
				product.checked = payload.checked;
			} else {
				product.checked = product.checked ? false : true;
			}

			uni.setStorage({
				key: 'cart',
				data: state.cartList,
				success: function() {
					// console.log('数据缓存成功');
				}
			});
		},

		//改变购物车中项目选中状态
		editAllCartItemStatus(state, payload) {
			// let product = state.cartList.find(item => item.product_id === payload.product_id);

			// 			console.log(JSON.stringify(payload))
			// 
			// 			console.log(JSON.stringify(state.cartList));

			for (let product of state.cartList) {
				product.checked = payload;
				// 				if (payload.hasOwnProperty('checked')) {
				// 					product.checked = payload.checked;
				// 				} else {
				// 					product.checked = product.checked ? false : true;
				// 				}
			}

			uni.setStorage({
				key: 'cart',
				data: state.cartList,
				success: function() {
					// console.log('数据缓存成功');
				}
			});

			console.log(JSON.stringify(state.cartList));
		},

		emptyCart(state) {
			state.cartList = [];
			uni.removeStorage({
				key: 'cart',
				success: function(res) {
					// console.log('清除数据缓存成功');
				}
			});
		},

		loadStorage(state) {
			uni.getStorage({
				key: 'cart',
				success: function(res) {
					// console.log(JSON.stringify(res.data));
					state.cartList = res.data;
				}
			});
		},
	},
	actions: {
		loadStorage(context) {
			// console.log('加载本地缓存的数据.....')
			return new Promise(resolve => {
				setTimeout(() => {
					context.commit('loadStorage');
					// console.log('数据加载完成.....')
					resolve();
				}, 500)
			});
		},

		clearStorage(context) {
			console.log('加载本地缓存的数据.....')
			return new Promise(resolve => {
				setTimeout(() => {
					context.commit('emptyCart'); //清除购物车
					context.commit('logout');
					resolve();
				}, 500)
			});
		},

		buy(context) {
			//提交服务器完成支付过程			
			return new Promise(resolve => {
				setTimeout(() => {
					context.commit('emptyCart');
					resolve();
				}, 500)
			});
		}
	},

	getters: {
		cartList: state => {
			return state.cartList;
		}
	},
})

export default store
