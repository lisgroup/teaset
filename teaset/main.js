import Vue from 'vue'
import App from '@/App'
import store from '@/store/index'
import request from '@/common/request'
import {
	logger,
	dump
} from '@/common/logger'

//常用组件，全局注册
import tsBadge from "@/components/teaset/components/ts-badge/ts-badge.vue";
import tsButton from "@/components/teaset/components/ts-button/ts-button.vue";
import tsBanner from "@/components/teaset/components/ts-banner/ts-banner.vue";
// import tsCityPicker from '@/components/teaset/components/ts-city-picker.vue';
// import tsDrawer from "@/components/teaset/components/ts-drawer.vue";
import tsFab from "@/components/teaset/components/ts-fab/ts-fab.vue";
// import tsFeedbackStar from '@/components/teaset/components/ts-feedback-star.vue';
import tsGap from '@/components/teaset/components/ts-gap/ts-gap.vue';

import tsIcon from "@/components/teaset/components/ts-icon/ts-icon.vue";
// import tsLeftCategory from '@/components/teaset/components/ts-left-category.vue';
import tsList from "@/components/teaset/components/ts-list/ts-list.vue";
import tsListItem from "@/components/teaset/components/ts-list/ts-list-item.vue";
import tsLoadMore from "@/components/teaset/components/ts-load-more/ts-load-more.vue";
import tsLine from '@/components/teaset/components/ts-line/ts-line.vue';
// import tsNoticeBar from "@/components/teaset/components/ts-notice-bar/ts-notice-bar.vue";
import tsPopup from "@/components/teaset/components/ts-popup/ts-popup.vue";
import tsPopupAd from "@/components/teaset/components/ts-popup/ts-popup-ad.vue";

import tsSearchBar from "@/components/teaset/components/ts-search-bar/ts-search-bar.vue";
import tsSegmentedControl from "@/components/teaset/components/ts-segmented-control/ts-segmented-control.vue";
import tsSection from '@/components/teaset/components/ts-section/ts-section.vue'
import tsSectionTitle from '@/components/teaset/components/ts-section/ts-section-title.vue'
import tsSectionBody from '@/components/teaset/components/ts-section/ts-section-body.vue'
import tsSectionFooter from '@/components/teaset/components/ts-section/ts-section-footer.vue'
// import tsSwipeAction from "@/components/teaset/components/ts-swipe-action/ts-swipe-action.vue";

import tsTag from "@/components/teaset/components/ts-tag/ts-tag.vue";
import tsTags from "@/components/teaset/components/ts-tag/ts-tags.vue";
import tsTagSelector from "@/components/teaset/components/ts-tag/ts-tag-selector.vue";
// import tsSteps from "@/components/teaset/components/ts-steps/ts-steps.vue";
// import tsTimeline from '@/components/teaset/components/ts-timeline/ts-timeline.vue';
// import tsTimelineItem from '@/components/teaset/components/ts-timeline/ts-timeline-item.vue';



//全局注册
Vue.component('ts-badge', tsBadge);
Vue.component('ts-banner', tsBanner);
Vue.component('ts-button', tsButton);
Vue.component('ts-fab', tsFab);
Vue.component('ts-gap', tsGap);
Vue.component('ts-icon', tsIcon);
Vue.component('ts-line', tsLine);
Vue.component('ts-load-more', tsLoadMore);
Vue.component('ts-list', tsList);
Vue.component('ts-list-item', tsListItem);
Vue.component('ts-popup', tsPopup);
Vue.component('ts-popup-ad', tsPopupAd);

Vue.component('ts-search-bar', tsSearchBar);
Vue.component('ts-segmented-control', tsSegmentedControl);
Vue.component('ts-section', tsSection);
Vue.component('ts-section-title', tsSectionTitle);
Vue.component('ts-section-body', tsSectionBody);
Vue.component('ts-section-footer', tsSectionFooter);
Vue.component('ts-tag', tsTag);
Vue.component('ts-tags', tsTags);
Vue.component('ts-tag-selector', tsTagSelector);


Vue.config.productionTip = false;
//挂载全局对象
Vue.prototype.$store = store
Vue.prototype.$request = request
Vue.prototype.$logger = logger; //日志记录器

logger.log(dump(store.state));


Vue.config.productionTip = false
Vue.prototype.$API_BASE=' https://www.easy-mock.com/mock/5bd6be9efffb9b57ff8cf3a7/teaset';

//#ifdef APP-PLUS
uni.getProvider({
	service: 'share',
	success: (e) => {
		// console.log('success', JSON.stringify(e.provider));
		// logger.log(e.provider);
		let data = []
		for (let i = 0; i < e.provider.length; i++) {
			switch (e.provider[i]) {
				case 'weixin':
					data.push({
						name: '分享到微信好友',
						id: 'weixin',
						icon: 'weixin',
						type: 'WXSceneSession', //default value
						sort: 0
					})
					data.push({
						name: '分享到微信朋友圈',
						id: 'weixin',
						icon: 'wechat',
						type: 'WXSenceTimeline',
						sort: 1
					})
					break;

				case 'qq':
					data.push({
						name: '分享到QQ',
						id: 'qq',
						icon: 'qq',
						sort: 2
					})
					break;
				case 'sinaweibo':
					data.push({
						name: '分享到新浪微博',
						id: 'sinaweibo',
						icon: 'sinaweibo',
						sort: 3
					})
					break;
				default:
					break;
			}
		}
		Vue.prototype.$providerList = data.sort((x, y) => {
			return x.sort - y.sort
		});
	},
	fail: (e) => {
		console.log('获取登录通道失败', e);
		//     uni.showModal({
		//       content: '获取登录通道失败',
		//       showCancel: false
		//     })
	}
});
//#endif

//注册全局组件
App.mpType = 'app'
const app = new Vue({
	store,
	request,
	...App
})
app.$mount()

