import request from 'common/request'
//直接下载整个markdown文本内容
const getMarkdownFileContent = async function(documentUrl) {
	let data = await request.get(documentUrl);
	return data;
}
/**
 * 获取实验基本信息
 */
const getLabList = async function() {
	let labs = [{
			group: '快速入门',
			lists: [{
					demoUrl: 'quickstart/hello-world/hello-world',
					meta: {
						title: 'Hello world',
						description: '我们先从一段简单的脚本开始，感受一下uni-app最核心的功能，uni-app是基于Vue.js的前端框架，支持大部分的 Vue.js特性。这个示例展示了uni-app的核心功能：数据的双向绑定。',
						documentUrl: 'http://www.zengqs.com/document/quickstart/hello-world/index.document.md',
						sourceUrl: 'http://www.zengqs.com/document/quickstart/hello-world/index.source.md',
						resourceUrl: 'http://www.zengqs.com/document/quickstart/hello-world/index.resource.md',
						videoUrl: ''
					}
				},
				{
					demoUrl: '',
					meta: {
						title: 'Es6基础教程',
						description: 'Es6基础教程!',
						documentUrl: 'http://www.zengqs.com/document/quickstart/es6/index.document.md',
						sourceUrl: '',
						resourceUrl: 'http://www.zengqs.com/document/quickstart/es6/index.resource.md',
						videoUrl: 'http://www.zengqs.com/document/video/media.mp4'
					}
				},
				{
					demoUrl: 'quickstart/layout/box-model-v1',
					meta: {
						title: 'CSS盒子模型基础',
						description: 'CSS盒子模型的基本概念',
						documentUrl: '',
						sourceUrl: '',
						resourceUrl: '',
						videoUrl: ''
					}
				},
				{
					demoUrl: 'quickstart/layout/box-model-v2',
					meta: {
						title: 'CSS盒子模型综合案例',
						description: 'CSS盒子模型综合案例',
						documentUrl: '',
						sourceUrl: '',
						resourceUrl: '',
						videoUrl: ''
					}
				}
			]
		},
		{
			group: '进阶',
			lists: [{
					demoUrl: 'special/product-list/product-list-v1',
					meta: {
						title: '产品列表 版本1',
						description: '演示如何从远程服务器获取JSON格式的列表数据，并使用v-for指令填充列表。',
						documentUrl: 'http://www.zengqs.com/document/lab/product-list/v1.document.md',
						sourceUrl: 'http://www.zengqs.com/document/lab/product-list/v1.source.md',
						resourceUrl: 'http://www.zengqs.com/document/lab/product-list/v1.resource.md',
						videoUrl: ''
					}
				},
				{
					demoUrl: 'special/product-list/product-list-v2',
					meta: {
						title: '产品列表 版本2',
						description: '演示如何从远程服务器获取JSON格式的列表数据，并使用v-for指令填充列表，使用条件编译实现H5APP与微信小程序的不同行为的处理。',
						documentUrl: 'http://www.zengqs.com/document/lab/product-list/v2.document.md',
						sourceUrl: 'http://www.zengqs.com/document/lab/product-list/v2.source.md',
						resourceUrl: 'http://www.zengqs.com/document/lab/product-list/v2.resource.md',
						videoUrl: ''
					}
				},
				{
					demoUrl: 'special/product-list/product-list-v3',
					meta: {
						title: '产品列表 版本3',
						description: '演示如何从远程服务器获取JSON格式的列表数据，并使用v-for指令填充列表，使用条件编译实现H5APP与微信小程序的不同行为的处理，实现上拉加载更多页面的数据。',
						documentUrl: 'http://www.zengqs.com/document/lab/product-list/v3.document.md',
						sourceUrl: 'http://www.zengqs.com/document/lab/product-list/v3.source.md',
						resourceUrl: 'http://www.zengqs.com/document/lab/product-list/v3.resource.md',
						videoUrl: ''
					}
				}
			]
		}
	];
	return labs;
}
/**
 * 获取课程信息
 */
const getCourseInfo = async function(courseId) {
	let course = {
		course_id: 1,
		cover_url: 'http://placehold.it/200x150',
		title: 'Uni-APP快速入门教程',
		tags: [
			'课堂录像','vue','uni-app','前端开发'
		],
		price: 49.9,
		origin_price: 99.9,
		description: '![](https://img.mukewang.com/5b3c2d6e0001241b09360316.jpg) \r\n' +
			'<img src="https://img.mukewang.com/5b3c2d6e0001241b09360316.jpg"/>',
		resource: '',
		teachers: [{
				name: '曾青松',
				students_in_learning: 300,
				description: "__曾青松，男，1976年，博士，教授、系统分析师。__\r\n" +
					"> 2003年5月获得高级程序员资格，2005年月10获得系统分析师资格，2012年12月获得计算机科学与技术副教授职称，2016年12月获得计算机应用教授职称。\r\n\r\n" +
					"他的研究领域为：\r\n" +
					"- 模式识别\r\n" +
					"- 机器学习\r\n" +
					"- 数据挖掘\r\n",
			},
			{
				name: '赵XX',
				students_in_learning: 400,
				description: "__赵XX，女，1983年，硕士，高级工程师。__\r\n" +
					"> 2003年5月获得高级程序员资格，2005年月10获得系统分析师资格，2012年12月获得计算机科学与技术副教授职称，2016年12月获得计算机应用教授职称。\r\n\r\n" +
					"他的研究领域为：\r\n" +
					"- 模式识别\r\n" +
					"- 机器学习\r\n" +
					"- 数据挖掘\r\n",
			},
		],
		chapters: [{
				title: '第一章 课程介绍',
				sections: [{
						title: '1-1 课程介绍',
						time: '05:07',
						videoUrl: 'http://www.zengqs.com/document/video/media.mp4',
						documentUrl: 'http://www.zengqs.com/document/quickstart/hello-world/index.document.md',
					},
					{
						title: '1-2 课程介绍',
						time: '05:07',
						videoUrl: 'http://www.zengqs.com/document/video/media.mp4',
						documentUrl: 'http://www.zengqs.com/document/quickstart/es6/index.document.md',
					}
				]
			},
			{
				title: '第二章 课程介绍',
				sections: [{
						title: '2-1 课程介绍',
						time: '05:07',
						videoUrl: 'https://www.dcloud.io/uniapp/wap2appvsnative.mp4',
						documentUrl: 'http://www.zengqs.com/document/quickstart/hello-world/index.document.md',
					},
					{
						title: '2-2 课程介绍',
						time: '05:07',
						videoUrl: 'https://www.dcloud.io/uniapp/wap2appvsnative.mp4',
						documentUrl: 'http://www.zengqs.com/document/quickstart/es6/index.document.md',
					}
				]
			}
		]
	}
	return course;
	// 	let api = `http://192.168.212.143:8360/api/course?course_id=${courseId}`;
	// 	let res = await request.get(api);
	// 	// console.log(JSON.stringify(res))
	// 	return res.data;
}
export default {
	getLabList,
	getCourseInfo,
	getMarkdownFileContent
}
