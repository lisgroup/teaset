import request from '@/common/request'

const getShopProducts = async function (shopId, page) {

	let url = `https://api.beidian.com/mroute.html?method=beidian.h5.shop.product.list&page=${page}&shop_id=682731`;
	const data = await request.get(url);
	
	// console.log(JSON.stringify(data));
	
	if (data.has_more) {
		return data.shop_products;
	} else {
		return false;
	}
}


const getTemaiProducts = async function (shopId, page) {

	let url = `https://api.beidian.com/mroute.html?method=beidian.h5.shop.product.list&page=${page}&shop_id=682731`;
	const data = await request.get(url);
	if (data.has_more) {
		return data.temai_products;
	} else {
		return false;
	}
}


const search = async function (keywords, page) {
	
	console.log("keywords:"+keywords)
	//https://api.beidian.com/mroute.html?method=beidian.search.item.list&keyword=%E7%94%B5%E8%84%91&sort=hot&page_size=20&page=3
	keywords = encodeURI(keywords);
	let url =
		`https://api.beidian.com/mroute.html?method=beidian.search.item.list&keyword=${keywords}&sort=hot&page_size=20&page=${page}`;
	console.log(url);
	const data = await request.get(url);

	// console.log('data:' + JSON.stringify(data));
	console.log(data.has_more)

	if (data.has_more) {
		return data.items;
	} else {
		// console.log("没有数据了！")
		return false;
	}
}

// app/store应用的服务程序
export default {
	getShopProducts,
	getTemaiProducts,
	search,
}

// import service from 'beidian-jianxuan.js'
// service.getShopProducts(....);


// import {getShopProducts,getTemaiProducts} from 'beidian-jianxuan.js'
// getShopProducts(....);